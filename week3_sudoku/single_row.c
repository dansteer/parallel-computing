#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int solve(int *ptr_sudoku, int *poss) {
	for(int i = 0; i < 9; i++) {
		if(*ptr_sudoku == 0 && *poss != 0) {
			*ptr_sudoku = *poss;
			poss++;
		}
		ptr_sudoku++;
	}
	return 0;
}

int findExisting(int *ptr_sudoku, int *pivot, int *poss) {
	for(int i = 0; i < 9; i++) {
		if(*ptr_sudoku == *pivot) {
			*pivot+=1;
			return 0;
		}
		ptr_sudoku++;
	}

	*poss = *pivot;
	*pivot+=1;
	return 0;	
}

int main() {

	int sudoku[9] = {0,0,1,0,0,2,0,0,3};
	int *poss = malloc(9 * sizeof(int)); 
	
	int count = 0;

	int *ptr_sudoku;
	int *pivot = malloc(sizeof(int));
	*pivot = 1;
	
	ptr_sudoku = sudoku;

	for(int i = 0; i < 9; i++) {
		findExisting(ptr_sudoku, pivot, poss);
		if(*poss != 0) {
			poss++;
			count++;
		}
	}

	printf("\nUnsolved:\n");
	for(int i = 0; i < 9; i++) 
		printf(" %d | ", sudoku[i]);

	poss-=count;
	solve(ptr_sudoku, poss);

	printf("\n\nSolved:\n");
	for(int i = 0; i < 9; i++) 
		printf(" %d | ", sudoku[i]);

	printf("\n");
	return 0;
}


