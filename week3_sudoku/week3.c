#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	int i, j, num;
	int sudokoArr[9] = {0};
	
	srand(time(NULL));

	for(i = 0;i < 9;i+=2) {
		num = rand() % 10;
		for(j = 0; j < 9; j+=2) {
			if(sudokoArr[j] != num) {
				sudokoArr[j] = num;
				break;
			}
		}
	}
	
	for(i = 0; i < 9; i++) {
		printf("%d\n", sudokoArr[i]);
	}
	return 0;
}
/*
line 
3 x 3 
whole grid;
*/