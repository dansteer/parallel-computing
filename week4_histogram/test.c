#include <stdio.h>
#include <omp.h>
void hello() {
	/* get thread id */
	int id = omp_get_thread_num();
	printf("Hello from thread %d\n", id);
}

int main() {
	#pragma omp parallel \
	num_threads(4)
	hello();
	return 0;
}