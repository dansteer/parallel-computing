#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

int splitNumbers(int *numbers) {
	char a[100] = "";
	char b[100] = "";
	char c[100] = "";
	char d[100] = "";
	char e[100] = "";
	char f[100] = "";
	char g[100] = "";
	char h[100] = "";
	char i[100] = "";
	char j[100] = "";

	for(int k = 0; k < 100; k++) {
		switch(*numbers) {
			case 0 ... 10:
				strcat(a, "*");
				break;
			case 11 ... 20:
				strcat(b, "*");
				break;

			case 21 ... 30:
				strcat(c, "*");
				break;

			case 31 ... 40:
				strcat(d, "*");
				break;
			
			case 41 ... 50:
				strcat(e, "*");
				break;

			case 51 ... 60:
				strcat(f, "*");
				break;

			case 61 ... 70:
				strcat(g, "*");
				break;

			case 71 ... 80:
				strcat(h, "*");
				break;

			case 81 ... 90:
				strcat(i, "*");
				break;

			case 91 ... 100:
				strcat(j, "*");
				break;

			default:
				printf("Error: number out of range\n");
				break;
		}
		numbers++;
	}

	printf("1 - 10   | %s\n", a);
	printf("11 - 20  | %s\n", b);
	printf("21 - 30  | %s\n", c);
	printf("31 - 40  | %s\n", d);
	printf("41 - 50  | %s\n", e);
	printf("51 - 60  | %s\n", f);
	printf("61 - 70  | %s\n", g);
	printf("71 - 80  | %s\n", h);
	printf("81 - 90  | %s\n", i);
	printf("91 - 100 | %s\n", j);
	return 0;
}


int readNumbersFromFile(int *numbers) {
	FILE *fptr;
	fptr = fopen("grades.txt", "r");
	for (int i = 0; i < 100; i++) {
		fscanf(fptr, "%d", numbers);
		numbers++;
	}
	fclose(fptr);
	return 0;
}

int main() {
	int *numbers = malloc(100 * sizeof(int));
	readNumbersFromFile(numbers);
	splitNumbers(numbers);
	return 0;
}