#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>

#define NUM_THREADS 2

int printHistogram(char **histogram) {
	for(int i = 0; i < 10; i++) {
		printf("Pos: %d Val: %s\n", i, *histogram);
		histogram++;
	}
	return 0;
}

int parallelPrepare(int *numbers, char **histogram) {
	int id;
	int count = 0;

	omp_set_num_threads(NUM_THREADS);
	#pragma omp parallel
	{
		id = omp_get_thread_num();
		
		for(int i = id; i < 100; i = i + NUM_THREADS) {
			count++;
			if(*numbers <= 10) {
				printf("It's over and :%d\n", *numbers);
				strcat(histogram[0], "*");
			}
			else if(*numbers > 10 && *numbers <= 20) {
				printf("It's over 10 and :%d\n", *numbers);
				strcat(histogram[1], "*");
			}
			else if(*numbers > 20 && *numbers <= 30) {
				printf("It's over 10 and :%d\n", *numbers);
				strcat(histogram[2], "*");
			}
			else if(*numbers > 30 && *numbers <= 40) {
				printf("It's over 10 and :%d\n", *numbers);
				strcat(histogram[3], "*");
			}
			else if(*numbers > 40 && *numbers <= 50) {
				printf("It's over 10 and :%d\n", *numbers);
				strcat(histogram[4], "*");
			}
			else if(*numbers > 50 && *numbers <= 60) {
				printf("It's over 10 and :%d\n", *numbers);
				strcat(histogram[5], "*");
			}
			else if(*numbers > 60 && *numbers <= 70) {
				printf("It's over 10 and :%d\n", *numbers);
				strcat(histogram[6], "*");
			}
			else if(*numbers > 70 && *numbers <= 80) {
				printf("It's over 10 and :%d\n", *numbers);
				strcat(histogram[7], "*");
			}
			else if(*numbers > 80 && *numbers <= 90) {
				printf("It's over 10 and :%d\n", *numbers);
				strcat(histogram[8], "*");
			}
			else if(*numbers > 90 && *numbers <= 100) {
				printf("It's over 10 and :%d\n", *numbers);
				strcat(histogram[9], "*");
			}
			numbers++; 
		}
 	}
 	printf("Count: %d\n", count);
	return 0;
}

int readNumbersFromFile(int *numbers) {
	FILE *fptr;
	fptr = fopen("grades.txt", "r");
	for (int i = 0; i < 100; i++) {
		fscanf(fptr, "%d", numbers);
		numbers++;
	}
	
	fclose(fptr);
	return 0;
}

int main() {
	int *numbers = malloc(100 * sizeof(int));
	char *histogram[10] = {""};
	for(int i = 0; i < 10; i++) {
		histogram[i] = malloc(100*sizeof(char));
	}
	readNumbersFromFile(numbers);
	parallelPrepare(numbers, histogram);
	printHistogram(histogram);
	return 0;
}