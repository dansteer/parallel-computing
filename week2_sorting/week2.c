#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef int bool;
#define true 1
#define false 0

int genNumbers() {
	int i;
	FILE *fptr;
	fptr = fopen("numbers.txt", "w");
	srand(time(NULL));
	for(i = 0;i < 1000; i++) 
		fprintf(fptr, "%d\n", rand() % 100);
	fclose(fptr);
	return 0;
}

int * readNumbersFromFile() {
	static int randomNumbers[1000], i;
	FILE *fptr;
	fptr = fopen("numbers.txt", "r");
	for(i = 0; i < 1000; i++) 
		fscanf(fptr, "%d", &randomNumbers[i]);
	fclose(fptr);
	return randomNumbers;
}

int bubbleSort(int *randomNumbers) {
	int i, j, swap;
	int size = 999; // TO STOP SEG FAULT, SET SIZE TO ONE
	for (i = 0 ; i < size; i++) {
		for (j = 0 ; j < size - i; j++) {
	  		if (randomNumbers[j] > randomNumbers[j+1]) {
			    swap = randomNumbers[j];
			    randomNumbers[j] = randomNumbers[j+1];
			    randomNumbers[j+1] = swap;
			 }
		}
	}

	for(i = 0; i < 1000; i++) 
		printf("%d\n", randomNumbers[i]);
	return 0;	
}

int	tester(int *randomNumbers) {
	for(int i = 0; i < 1; i++) {
		printf("tester func: %d\n", randomNumbers[i]);
	}
	randomNumbers[0] = 999;
}

int partition(int *randomNumbers, int first, int last) {

	bool INDEX_FOUND = false;
	bool runningLoop = false;
	int wall, swap, swap2;
	int pivot = randomNumbers[last];
	int swapped = false;
	int jobComplete = false;

	while(!runningLoop) {	
		printf("current: %d  last: %d\n", randomNumbers[first], randomNumbers[last]);
		if(randomNumbers[first] > randomNumbers[last]) {
			printf("pivot is bigger\n");
			if(!INDEX_FOUND) {
				wall = first;
				INDEX_FOUND = true;
				printf("setting wall\n");
			}
		}
		else if (randomNumbers[first] < randomNumbers[last]) {
			swap = randomNumbers[first];
			swap2 = wall;
			printf("swapping %d for %d\n", randomNumbers[wall], randomNumbers[first]);
			randomNumbers[wall] = randomNumbers[first];
			randomNumbers[first] = randomNumbers[swap2];
			wall++;
			swapped	= true;
		}

		if (first == 999 && swapped == false) {
			printf("no swapping happened this time\n");
			runningLoop = true;
			jobComplete = true;
			return jobComplete;
		}
		else if(first == 999) {
			if(randomNumbers[first] == randomNumbers[last]) {
				printf("current is last!!!\n");
				swap = randomNumbers[wall+1];
				randomNumbers[wall] = randomNumbers[last];
				randomNumbers[last] = swap;
				printf("fucking swapping happened\n");
			}
			runningLoop = true;
			return jobComplete;
		}




		if(first < 999) first++;
		
	}	
	return jobComplete;
}

int quickSort(int *randomNumbers) {
	int first = 0, last = 999;
	bool finish = false;

	partition(randomNumbers, first, last);
	int count = 0;
	
	for(int i = 0; i < 999; i++) {
		if(randomNumbers[i] > randomNumbers[last]) {
			count++;
		}
	}
	
	if (count != 0) {
		printf("%d incomplete\n", count);
		quickSort(randomNumbers);
	}
	
	

	


	for(int i = 0; i < 1; i++) {
		printf("quicksort bc: %d\n", randomNumbers[i]);
	}

	for(int i = 0; i < 999; i++) {
		//printf("%d\n", randomNumbers[i]);
	}

	tester(randomNumbers);

	//printf("round two quick: %d\n", randomNumbers[0]);
	//printf("Size of array %ld\n", sizes/sizeof(int));
	return 0;
}

int main() {
	int *randomNumbers, i, j, swap;
	genNumbers();
	readNumbersFromFile();
	randomNumbers = readNumbersFromFile();
	//bubbleSort(randomNumbers);
	quickSort(randomNumbers);
	//printf("in main: %d\n", randomNumbers[0]);
	return 0;
}

