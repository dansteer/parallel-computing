#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	int nolines, nocol, a, b, c, row, col, count, sum;
	int first_m_row = 50, first_m_col = 70, second_m_row = 70, second_m_col = 80;	
	int first_matrix[first_m_row][first_m_col], second_matrix[second_m_row][second_m_col];

	// SMALL ARRAYS FOR EASY TESTING
	int test_array1[3][3] = {
		{3,4,5},
		{2,4,6},
		{3,6,9}
	};

	int test_array2[3][3] = {
		{3,4,5},
		{2,4,6},
		{3,6,9}
	};

	int test_array3[3][3] = {0};
	int result_matrix[70][70] = {0};
	

	FILE *fptr;
	fptr = fopen("numbers.txt", "w");

	// SEED WITH TIME TO GET RANDOM NUMBERS
	//srand ( time(NULL) );

	// EST. FIRST MATRIX WITH RANDOM NUMBERS
	count = 0;
	for (a = 0; a<first_m_row; a++) { 
		for (b = 0; b<first_m_col; b++) {
			first_matrix[a][b] = rand() % 50;
			count++;
		}
	}
	
	// EST.SECOND MATRIX WITH RANDOM NUMBERS
	count = 0;
	for (a = 0; a<second_m_row; a++) { 
		for (b = 0; b<second_m_col; b++) {
			second_matrix[a][b] = rand() % 50;
			count++;
		}
	}

	// TESTING 

	/*- PRINT EACH ELEMENTS FROM ARRAY 
	
	/*for(row=0;row<first_m_row; row++) {
		for(col = 0;col<first_m_col;col++) {
			//printf("Row %d     \n", first_matrix[row][col]);
		}
	}
	
	// TEST MULTIPLY ON TEST ARRAYS
	for(a = 0;a<3;a++) {
		for(b = 0;b<3;b++) {
			for(c = 0;c<3;c++) {
				test_array3[a][b] += test_array1[a][c] * test_array2[c][b];
			}
			printf("%d\n", test_array3[a][b]);
		}
	}
	*/
	// FINAL: MULTIPLY TWO LARAGE MATRICIES

	count = 0;
	for(a = 0;a<50;a++) {
		for(b = 0;b<80;b++) {
			for(c = 0;c<70;c++) {
				result_matrix[a][b] += first_matrix[a][c] * second_matrix[c][b];
				count++;
			}
			printf("%d\n", result_matrix[a][b]);
		}
	}

	// CALCULATE FIRST ITEM IN RESULT ARRAY 
	sum = 0;
	for(a=0;a<70;a++) {
		//if(a==0) 
			//printf("%d\n", first_matrix[0][a]);
		sum += first_matrix[0][a] * second_matrix[a][0];
	}

	int result_test[70][70] = {0};
	//result_test = {0};
	sum = 0;
	for(a=0;a<70;a++) {
		//if(a==0) 
			//printf("%d\n", first_matrix[0][a]);
		sum += first_matrix[0][a] * second_matrix[a][0];
		result_test[a][a] = sum;
		//if (a<5)
			//printf("%d\n", result_test[a][a]);

	}

	/*for(a=0;a<70;a++) {
		printf("%d\n", result_matrix[a][a]);
	}*/

	printf("Result matrix 0x0: %d\n", result_matrix[0][0]);
	printf("Calculated result 0x0: %d\n", sum);
	

	return 0;
}