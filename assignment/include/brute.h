/*!
 * @brief Define constants that will be used by all implementations of the brute force.
 * @author Dan Steer
 * @date 03/12/2018
 */

#ifndef __BRUTE_H__
#define __BRUTE_H__

#define RANGE_START 97 // 'a'
#define RANGE_END 115 // 's'

#define SPECTRUM 893871739 // THIS IS THE ITERATIONS IT TAKES TO COMPLETE A RANGE SEARCH e.g. a#######->, b#######->

#endif /* __BRUTE_H__ */