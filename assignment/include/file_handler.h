/*!
 * @brief Functions that will be used for handling the encypted text file.
 * @author Dan Steer
 * @date 05/12/2018
 */

#ifndef __FILE_HANDLER_H__
#define __FILE_HANDLER_H__

/*!
 * @brief Create an encrypted text file
 * @param data The data that will be wrriten to the file (will be pass the cipher text)
 * @param length The length of the cipher text
 */
int CreateEncryptedFile(unsigned char* data, int* length) {
	FILE *fp;
	fp = fopen("encrypted_text_file", "wb"); // Open/create a file to store the cipher text.
	fwrite(data, 1, *length, fp); // Write cipher text to the file.
	fclose(fp);
}

/*!
 * @brief Read from contents of the encrypted text file, and return.
 * @param length The length of the cipher text
 * @return buffer The contents of the encrypted text file
 */
char * DecryptFile(int* length) {
	unsigned char *buffer = malloc(128); // Define variable to store the contents of the file
	FILE *fp;	
	fp = fopen("encrypted_text_file", "r"); // Open the file in read only mode.
	fread(buffer, 1, *length, fp); // Read the contents out.
	fclose(fp);
	return buffer; // Return the contents of the encrypted text file.
}

#endif /* __FILE_HANDLER_H__ */