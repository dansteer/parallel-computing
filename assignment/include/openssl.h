/*!
 * @brief Modified functions to encrypt/decrypt with OpenSSL
 * @author Dan Steer
 * @date 05/12/2018
 */

#ifndef __OPENSSL_H__
#define __OPENSSL_H__

#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include "../include/file_handler.h"

/*!
 * @brief Encrypt the the plain text
 * @param key The key used to encrypt the text file
 * @param outlen The length of the cipher text
 */
int Decrypt(char key[17], int *outlen) {
	char iv[17] = "0123456789012345"; // Initilisation vector
	unsigned char* plainText = malloc(128); // Variable to store the output of the decrypt (plain text) (allocate 128 bytes)
	unsigned char* encryptedText = malloc(128); // Variable to store the encrypted text from the text file
	int len, decbuflen;

	EVP_CIPHER_CTX *ctx;
	ctx = EVP_CIPHER_CTX_new();

	/* DecryptFile returns the encrypted text from encypted text file. This value is stored in the
	encryptedText defined above.*/
	strcpy(encryptedText, DecryptFile(outlen)); 

	if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv)) { // Start the decrypt
		printf("Error in EVP_DecryptInit_ex\n"); // Print error if this fails.
		return 0;
	}

	if(1 != EVP_DecryptUpdate(ctx, plainText, &len, encryptedText, *outlen)) {
		printf("Error in: EVP_DecryptUpdate\n"); // Print error if this fails.
		return 0;
	}

	decbuflen = len; // Store the length of the decrypted text

	if(1 != EVP_DecryptFinal_ex(ctx, plainText + len, &len)) 
		printf("Error in: EVP_DecryptFinal_ex \n"); // Print error if this fails.
	else 
		printf("PLAIN TEXT: %s\n", plainText); // Print result of the decryption (plain text)
}

/*!
 * @brief Encrypt the the plain text
 * @param key The key used to encrypt the text file
 * @param outlen The length of the cipher text
 */
int Encrypt(char key[17], int* outlen) {
	char intext[128] = "encrypt this text"; // The plain text to be encrypted
	char iv[17] = "0123456789012345"; // Initilisation vector
	unsigned char *output = malloc(128); // Variable to store the output of the encrypt (allocate 128 bytes)
	int tmplen;

	EVP_CIPHER_CTX *ctx;
	ctx = EVP_CIPHER_CTX_new();

	EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv); // Init the encryption

	if(!EVP_EncryptUpdate(ctx, output, outlen, intext, strlen(intext))) { // Start encryption
		printf("Error in EVP_EncryptUpdate\n"); // Return error if this fails
		EVP_CIPHER_CTX_free(ctx);
		exit(0);
	}

	if(!EVP_EncryptFinal_ex(ctx, output + *outlen, &tmplen)) { 
		printf("Error in FInal_ex\n"); // Return error if this fails
		EVP_CIPHER_CTX_free(ctx);
		exit(0);
	}
	
	*outlen += tmplen; // Store the total length of the encrypted text

	CreateEncryptedFile(output, outlen); // Write output to file

	printf("ENCRYPTED TEXT IS: %s\n", output);

	return 0;
}

#endif /* __OPENSSL_H__ */