/*!
 * @brief The MPI implementation for brute forcing a cipher.
 * @author Dan Steer
 * @date 06/12/2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <mpi.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include "../include/brute.h"
#include "../include/openssl.h"

/*!
 * @brief Encrypt the the plain text
 * @param key The key used to encrypt the text file
 * @param outlen The length of the cipher text
 * @param argc The number of nodes
 * @param argv Arguements passed
 */
int EncryptTwo(char key[17], int* outlen, int* argc, char** argv) {
	char intext[128] = "encrypt this text"; // The plain text to be encrypted
	char iv[17] = "0123456789012345"; // Initilisation vector
	unsigned char* output = malloc(128); // Variable to store the output of the encrypt (allocate 128 bytes)
	int tmplen;
	FILE *out; // File pointer

	EVP_CIPHER_CTX *ctx;
	ctx = EVP_CIPHER_CTX_new();

	EVP_EncryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, key, iv); // Init the encryption

	if(!EVP_EncryptUpdate(ctx, output, outlen, intext, strlen(intext))) { // Start encryption
		printf("Error in EVP_EncryptUpdate\n"); // Return error if this fails
		EVP_CIPHER_CTX_free(ctx);
		exit(0);
	}

	if(!EVP_EncryptFinal_ex(ctx, output + *outlen, &tmplen)) { // Encrypt the final part of the plain text 
		printf("Error in FInal_ex\n"); // Print error if this fails.
		EVP_CIPHER_CTX_free(ctx);
		exit(0);
	}
	
	*outlen += tmplen; // Store the total length of the encrypted text
	
	out = fopen("encrypted_text_file", "wb"); // Open/create a file to store the cipher text.
	fwrite(output, 1, *outlen, out); // Write cipher text to the file.
	fclose(out);
	
	int rank;
	MPI_Init (argc, &argv); // Init MPI
	MPI_Comm_rank (MPI_COMM_WORLD, &rank); // Get current rank and return to variable 'rank'
	if(rank == 0)
		printf("ENCRYPTED TEXT IS: %s\n", output); // Print message once
	
	return 0;
}

/*!
 * @brief Generate combinations of keys, and test against the actual key used for encryption
 * @param keyToBreak The key used to encrypt the text file
 * @param outlen The length of the cipher text
 */
int BruteForce(char keyToBreak[17], int* outlen) {
	int i, j, k, l, m, n, o, p; // Incrementer values local to each rank.
	char keyGen[17] = "################"; // Start with a blank key
	
	int rank, mpiSize;	
	MPI_Comm_rank (MPI_COMM_WORLD, &rank);
	MPI_Comm_size (MPI_COMM_WORLD, &mpiSize);
	
	if(rank == 0)
		printf("BRUTE FORCING...\n"); // Print message only once.

	clock_t start = clock(); // Start timer
	int counter = 0; // Counts how many times a key is generated. Local to the rank.
	int extendRange = 0; // This will be used to ensure ranks run on seperate key combinations. Local to the rank.
	
	for(i = RANGE_START; i <= RANGE_END; i++) {
		for(j = RANGE_START; j <= RANGE_END; j++) {
			for(k = RANGE_START; k <= RANGE_END; k++) {
				for(l = RANGE_START; l <= RANGE_END; l++) {
					for(m = RANGE_START; m <= RANGE_END; m++) {
						for(n = RANGE_START; n <= RANGE_END; n++) {
							for(o = RANGE_START; o <= RANGE_END; o++) {
								for(p = RANGE_START; p <= RANGE_END; p++) {
									

									/* This makes each rank have a different start position. Position [0] takes the longest time to complete
									so it makes sense for this to be ran in different ranks. The extend range variable (initially 0) will increase 
									by the number of ranks, so this will function with any given number ranks. This means each rank will run 
									on different rows, so a key is never generated twice (which makes the code more efficient), but it's also exhaustive,
									and will try every combination to ensure a key is found.*/
									keyGen[0] = i + rank + extendRange;
									keyGen[1] = j;
									keyGen[2] = k;
									keyGen[3] = l;
									keyGen[4] = m;
									keyGen[5] = n;
									keyGen[6] = o;
									keyGen[7] = p;
									
									counter++; // Increment the counter for each rank independently 
									
									/* SPECTURM is the number of interations to complete a row. When the local thread reaches this number, it changes the
									start position based on number of threads being used.*/
									if(counter == SPECTRUM) {
										extendRange += mpiSize - 1;
										counter = 0; // Zero the local rank counter.
									}									
	
									if(strcmp(keyToBreak, keyGen) == 0) { // If the generated matches the key used for encryption
										printf("Found the key! Found by rank: %d\nTime taken: %f seconds\n", rank, ((double) (clock() - start)) / CLOCKS_PER_SEC);
										printf("DECODING...\n");
										Decrypt(keyToBreak, outlen); // Call the decrypt function, pass in the key genereated by the brute force
										exit(0);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return 0;
}

/*!
 * @brief Define the key used for encryption, encrypt the plain text and start the brute force.
 * @param argc The number of nodes
 * @param argv Arguements passed
 */
int main(int argc, char* argv[]) {
	char keyToBreak[17] =  "fffffffe########";
	int outlen = 0;
	EncryptTwo(keyToBreak, &outlen, &argc, argv);
	BruteForce(keyToBreak, &outlen);		
	return 0;
}