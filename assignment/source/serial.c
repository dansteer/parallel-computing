/*!
 * @brief The serial implementation for brute forcing a cipher.
 * @author Dan Steer
 * @date 06/12/2018
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <omp.h>

#include "../include/brute.h"
#include "../include/openssl.h"
#include "../include/file_handler.h"


/*!
 * @brief Generate combinations of keys, and test against the actual key used for encryption
 * @param keyToBreak The key used to encrypt the text file
 * @param outlen The length of the cipher text
 */

int BruteForce(char keyToBreak[17], int* outlen) {
	int i, j, k, l, m, n, o, p;
	char keyGen[17] = "################"; // Start with a blank key
	printf("BRUTE FORCING...\n");
	clock_t start = clock(); // Start timer
	for(i = RANGE_START; i <= RANGE_END; i++) {
		for(j = RANGE_START; j <= RANGE_END; j++) {
			for(k = RANGE_START; k <= RANGE_END; k++) {
				for(l = RANGE_START; l <= RANGE_END; l++) {
					for(m = RANGE_START; m <= RANGE_END; m++) {
						for(n = RANGE_START; n <= RANGE_END; n++) {
							for(o = RANGE_START; o <= RANGE_END; o++) {
								for(p = RANGE_START; p <= RANGE_END; p++) {

									keyGen[0] = i;
									keyGen[1] = j;
									keyGen[2] = k;
									keyGen[3] = l;
									keyGen[4] = m;
									keyGen[5] = n;
									keyGen[6] = o;
									keyGen[7] = p;
									
									if(strcmp(keyToBreak, keyGen) == 0) { // If the generated matches the key used for encryption
										printf("Found the key! Time taken: %f seconds\n", ((double) (clock() - start)) / CLOCKS_PER_SEC); // Print time taken is seconds
										printf("DECODING...\n");
										Decrypt(keyToBreak, outlen); // Call the decrypt function, pass in the key genereated by the brute force
										return 0;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return 0;
}


/*!
 * @brief Define the key used for encryption, encrypt the plain text and start the brute force.
 */

int main() {
	char keyToBreak[17] =  "effffffb########"; // This is used to encrypt the file. It can be 'a' to 's'
	int outlen = 0; // Length of the cipher text
	Encrypt(keyToBreak, &outlen);
	BruteForce(keyToBreak, &outlen);
	return 0;
}