# **Parallel Computing Assignment**
##### This directory includes three source files: a serial, OpenMP and MPI implementation of a cipher brute-force.

### **Instructions**:
1.  Run `make`
This will create three executable files: **serial**, **openmp** and **mpi**.
2. The Serial and OpenMP variations of the brute-force program can be launch by running `./serial` and `./openmp` respectively.
3. Running MPI can be executed as follows: `mpirun -quiet -np 4 ./mpi`


##### **Notes**:
Because of how MPI has been implemented, the `-quiet` flag mutes warnings. This is because the program is being ended via an `exit(0)` command. The correct way of ending an MPI program on a cluster would be to broadcast to the nodes and end.