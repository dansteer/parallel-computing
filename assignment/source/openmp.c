/*!
 * @brief The OpenMP implementation for brute forcing a cipher.
 * @author Dan Steer
 * @date 06/12/2018
 */

#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "../include/brute.h"
#include "../include/openssl.h"

/*!
 * @brief Generate combinations of keys, and test against the actual key used for encryption
 * @param keyToBreak The key used to encrypt the text file
 * @param outlen The length of the cipher text
 */
int BruteForce(char keyToBreak[17], int* outlen) {
	printf("BRUTE FORCING...\n");
	clock_t start = clock(); // Start timer
	#pragma omp parallel // Parallel structure block starts
	{
		int numOfThreads = omp_get_num_threads(); // Get number of threads available. Default is 4.
		char keyGen[17] = "################"; // Start with a blank key
		int i, j, k, l, m, n, o, p; // Incrementer values local to each thread.
		int id = omp_get_thread_num(); // Get id of the active thread
		int extendRange = 0; // This will be used to ensure threads run on seperate key combinations. Local to the thread.
		int counter = 0; // Counts how many times a key is generated. Local to the thread.
		for(i = RANGE_START; i <= RANGE_END; i++) {
			for(j = RANGE_START; j <= RANGE_END; j++) {
				for(k = RANGE_START; k <= RANGE_END; k++) {	
					for(l = RANGE_START; l <= RANGE_END; l++) {		
						for(m = RANGE_START; m <= RANGE_END; m++) {			
							for(n = RANGE_START; n <= RANGE_END; n++) {
								for(o = RANGE_START; o <= RANGE_END; o++) {		
									for(p = RANGE_START; p <= RANGE_END; p++) {	

										/* This makes each thread have a different start position. Position [0] takes the longest time to complete
										so it makes sense for this to be ran in different threads. The extend range variable (initially 0) will increase 
										by the number of threads (- 1), so this will function with any given number threads. This means each thread will run 
										on different rows, so a key is never generated twice (which makes the code more efficient), but it's also exhaustive,
										and will try every combination to ensure a key is found.*/
										keyGen[0] = i + id + extendRange;
										keyGen[1] = j;
										keyGen[2] = k;
										keyGen[3] = l;
										keyGen[4] = m;
										keyGen[5] = n;
										keyGen[6] = o;
										keyGen[7] = p;

										counter++; // Increment the counter for each thread independently 
										
										/* SPECTURM is the number of interations to complete a row. When the local thread reaches this number, it changes the
										start position based on number of threads being used.*/
										if(counter == SPECTRUM) { 
											extendRange += numOfThreads - 1;
											counter = 0; // Zero the local thread counter.
										}

										if(strcmp(keyToBreak, keyGen) == 0) { // If the generated matches the key used for encryption
											printf("Found the key! Found by thread: %d\nTime taken: %f seconds\n", id, ((double) (clock() - start)) / CLOCKS_PER_SEC / numOfThreads);
											printf("DECODING...\n");
											Decrypt(keyToBreak, outlen); // Call the decrypt function, pass in the key genereated by the brute force
											exit(0);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return 0;
}

/*!
 * @brief Define the key used for encryption, encrypt the plain text and start the brute force.
 */
int main() {
	char keyToBreak[17] =  "fffffffe########"; // This is used to encrypt the file. It can be 'a' to 's'
	int outlen = 0; // Length of the cipher text
	Encrypt(keyToBreak, &outlen);
	BruteForce(keyToBreak, &outlen);
	return 0;
}
